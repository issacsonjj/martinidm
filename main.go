// MartiniDm project main.go
package main

import (
	"fmt"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"net/http"
)

func main() {
	fmt.Println("start")
	m := martini.Classic()
	m.Get("/", func() string {
		return "hii GET!"
	})

	m.Get("/hi", func() (int, string) {
		return 400, "hiiiiiii"
	})

	m.Get("/hello/:name/:name1", func(params martini.Params) string {
		return "Hello " + params["name"] + ", " + params["name1"]
	})

	m.Get("/hi/**", func(params martini.Params) string {
		return "Hi " + params["_1"]
	})

	m.Get("/raw", func(res http.ResponseWriter, req *http.Request) {
		//http.Redirect(res, req, "/books/buy/jjj", 302) //redirect
		//res.Write([]byte("hiii raw"))

	})

	m.Group("/books/buy", func(r martini.Router) {
		r.Get("/:id", func(params martini.Params) string {
			return "buy " + params["id"]
		})
	})

	/*
		m.Use(func(res http.ResponseWriter, req *http.Request) {
			res.WriteHeader(http.StatusUnauthorized)
			res.Write([]byte("unauthorized"))
		})
	*/

	m.Use(func(c martini.Context) {
		fmt.Println("before a request")

		c.Next()

		fmt.Println("after a request")
	})

	op := martini.StaticOptions{Prefix: "/static", SkipLogging: true, IndexFile: "", Expires: func() string { return "" }}
	m.Use(martini.Static("assets", op))
	m.Use(render.Renderer())
	mp := make(map[string]string)
	mp["current"] = "xjj"
	m.Get("/index", func(r render.Render) {
		r.HTML(200, "index", mp)
	})

	m.Run()
}
